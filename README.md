Run the script **detect_bright_areas.py** with input argument ***-i*** as the thermal
image to be processed.

Demo image:

![Alt text](Chilicotin/DJI_0041.jpg?raw=true "Title")

Output:

![Alt text](thermal_hotspots_demo.png?raw=true "Title")
