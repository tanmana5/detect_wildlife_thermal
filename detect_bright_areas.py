"""Script to detect hot spots in thermal image input."""
from imutils import contours
from skimage import measure
import numpy as np
import argparse
import imutils
import cv2

# threshold params
LOW_RANGE = 150
HIGH_RANGE = 255
MIN_NUM_PIXELS = 5


def preprocess_thermal_image(image):
    """Input args:
        - blurred - preprocessed image
       Returns:
        - cnts - contours of detected bright regions
    """
    # convert it to grayscale, and blur to remove noise
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    return blurred


def find_contours(blurred):
    """Input args:
        - blurred - preprocessed image
       Returns:
        - cnts - contours of detected bright regions
    """
    # threshold the image to reveal light regions in the blurred image
    thresh = cv2.threshold(blurred, LOW_RANGE, HIGH_RANGE, cv2.THRESH_BINARY)[1]
    # perform a connected component analysis on the thresholded
    # image, then initialize a mask to store regions detected
    labels = measure.label(thresh, neighbors=4, background=0)
    mask = np.zeros(thresh.shape, dtype='uint8')
    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue
        # otherwise, construct the label mask and count the
        # number of pixels
        labelMask = np.zeros(thresh.shape, dtype='uint8')
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
        # if the number of pixels in the component is sufficiently
        # large, then add it to mask of 'blobs'
        if numPixels >= MIN_NUM_PIXELS:
            mask = cv2.add(mask, labelMask)
    # find the contours in the mask, then sort them from left to
    # right
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = contours.sort_contours(cnts)[0]
    return cnts


if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--image', required=True,
                    help='path to the image file')
    args = vars(ap.parse_args())
    image = cv2.imread(args['image'])
    blurred = preprocess_thermal_image(image)
    cnts = find_contours(blurred)
    cv2.imshow('Original image', image)
    # loop over the contours
    for (i, c) in enumerate(cnts):
        # draw the bright spot on the image
        (x, y, w, h) = cv2.boundingRect(c)
        ((cX, cY), radius) = cv2.minEnclosingCircle(c)
        cv2.circle(image, (int(cX), int(cY)), int(radius),
                          (0, 0, 255), 3)
        cv2.putText(image, '#{}'.format(i + 1), (x, y - 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
        print('Hotspot located at {}'.format(int(cX), int(cY)))
    # show the output image with detections visualized
    cv2.imshow('Image', image)
    cv2.waitKey(0)
